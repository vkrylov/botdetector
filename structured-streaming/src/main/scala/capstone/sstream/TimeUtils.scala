package capstone.sstream

import com.typesafe.config.Config

object TimeUtils {

  implicit class ConfigOps(c: Config) {
    def sparkSeconds(p: String): String = {
      c.getDuration(p).getSeconds + " seconds"
    }
  }
}
