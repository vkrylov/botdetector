package capstone.sstream

import com.typesafe.config.Config

case class WindowSettings(window: String, slide: String, watermark: String)

object WindowSettings {
  def apply(config: Config): WindowSettings = {
    import TimeUtils._

    new WindowSettings(
      window = config sparkSeconds "window",
      slide = config sparkSeconds "slide",
      watermark = config sparkSeconds "watermark"
    )
  }
}