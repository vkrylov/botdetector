package capstone.sstream

import java.sql.Timestamp

import capstone.domain.Domain
import capstone.domain.Domain.BotItem
import com.datastax.driver.core.querybuilder.QueryBuilder
import com.datastax.spark.connector.cql.CassandraConnector
import com.typesafe.config.Config
import org.apache.spark.sql.streaming.{DataStreamWriter, OutputMode}
import org.apache.spark.sql.{Dataset, ForeachWriter, SparkSession}

import scala.collection.JavaConversions._

trait StreamWriter {

  def writer(bots: Dataset[Domain.BotItem])(implicit spark: SparkSession): DataStreamWriter[Domain.BotItem]

}

object StreamWriter {
  val cassandraWriter: Config => StreamWriter = { config =>
    new StreamWriter {
      def writer(bots: Dataset[Domain.BotItem])(implicit spark: SparkSession) = {
        val writer = new ForeachWriter[BotItem] {
          spark.sparkContext.getConf
            .set("spark.cassandra.connection.host", config getStringList "cassandra.hosts" mkString ",")
            .set("spark.cassandra.connection.port", config getString "cassandra.port")

          private val connector = CassandraConnector(spark.sparkContext.getConf)
          private val names = List("ip", "cause", "detected_timestamp").toArray

          def open(partitionId: Long, version: Long) = true

          def process(value: BotItem): Unit = processRow(value)

          def close(errorOrNull: Throwable): Unit = {}

          def processRow(value: BotItem): Unit = {
            connector.withSessionDo { session =>
              val values = List[AnyRef](value.ip, value.cause, new Timestamp(value.detected_timestamp * 1000)).toArray
              val insert = QueryBuilder.insertInto("detector", "bot").values(names, values)
              session.execute(insert)
            }
          }
        }

        bots.writeStream
          .outputMode(OutputMode.Complete)
          .foreach(writer)
      }
    }
  }

  val parquetWriter: StreamWriter = new StreamWriter {
    def writer(bots: Dataset[Domain.BotItem])(implicit spark: SparkSession) = {
      bots.writeStream
        .outputMode(OutputMode.Append)
        .partitionBy("ip")
        .format("parquet")
        .option("path", "./parquet")
    }
  }

  val consoleWriter: StreamWriter = new StreamWriter {
    def writer(bots: Dataset[Domain.BotItem])(implicit spark: SparkSession) = {
      bots.writeStream
        .outputMode(OutputMode.Update)
        .format("console")
        .option("truncate", value = false)
        .option("numRows", 50)
    }
  }
}
