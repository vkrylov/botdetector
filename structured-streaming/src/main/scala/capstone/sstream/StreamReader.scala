package capstone.sstream

import capstone.domain.Domain.Event
import com.typesafe.config.Config
import org.apache.spark.sql.functions.from_json
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.{Dataset, Encoders, SparkSession}

import scala.collection.JavaConversions._

object StreamReader {

  type StreamReader = SparkSession => Dataset[Event]

  val kafkaReader: Config => StreamReader = { config =>
    val kafkaConfig = config getConfig "kafka"
    val bootstrapServers = kafkaConfig getStringList "bootstrap.servers" mkString ","
    val topics = kafkaConfig getStringList "topics" mkString ","

    spark => {
      import spark.implicits._

      val eventSchema = Encoders.product[Event].schema

      val df = spark
        .readStream
        .format("kafka")
        .option("kafka.bootstrap.servers", bootstrapServers)
        .option("subscribe", topics)
        .option("startingOffsets", "earliest")
        .load()

      df.select(from_json('value cast StringType, eventSchema).as[Event])
    }
  }

  val csvReader: Config => StreamReader = { config => spark =>
    val conf = config getConfig "csv"
    val dir = conf getString "dir"
    val withHeader = conf getBoolean "withHeader"
    val eventSchema = Encoders.product[Event].schema
    import spark.implicits._

    spark.readStream
      .option("header", withHeader.toString)
      .schema(eventSchema)
      .csv(dir)
      .as[Event]
  }
}
