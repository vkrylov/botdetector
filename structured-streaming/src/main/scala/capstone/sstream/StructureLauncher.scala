package capstone.sstream

import com.typesafe.config.ConfigFactory
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

object StructureLauncher extends App {
  val config = ConfigFactory.load getConfig "capstone.botdetector"
  val checkpoint = config getString "checkpoint.dir"

  val sc = new SparkConf(true)
    .setMaster("local[*]")
    .setAppName("BotDetector")
    .set("spark.sql.shuffle.partitions", "3")
    .set("spark.sql.streaming.checkpointLocation", checkpoint)

  implicit val spark = SparkSession.builder.config(sc).getOrCreate()

  val sourceConfig = config getConfig "source"
  val events = StreamReader.kafkaReader apply sourceConfig apply spark

  val bots = StructureStreamingDetector(config).run(events)

  StreamWriter.consoleWriter
    .writer(bots)
    .start()
    .awaitTermination()
}