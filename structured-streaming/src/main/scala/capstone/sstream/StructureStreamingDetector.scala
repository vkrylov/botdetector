package capstone.sstream

import capstone.domain.Domain.{BotItem, Event}
import com.typesafe.config.Config
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._


class StructureStreamingDetector(detectionSettings: DetectionSettings, windowSettings: WindowSettings) {

  def run(df: Dataset[Event]): Dataset[BotItem] = {
    import df.sqlContext.implicits._

    df
      .withColumn("timestamp", 'unix_time cast LongType cast TimestampType)
      .drop('unix_time)
      .withColumn("clickCount", when('type === "click", 1) otherwise 0)
      .withColumn("viewCount", when('type === "view", 1) otherwise 0)
      .drop('type)
      .withWatermark("timestamp", windowSettings.watermark)
      .groupBy('ip, window('timestamp, windowSettings.window, windowSettings.slide))
      .agg(
        min('timestamp) as "start",
        max('timestamp) as "end",
        sum('clickCount) as "clickCount",
        sum('viewCount) as "viewCount"
      )
      .withColumn("total", 'clickCount + 'viewCount)
      .withColumn("clickRate", 'clickCount / ('total cast DoubleType))
      .withColumn("delta", ('end cast LongType) - ('start cast LongType) + 1)
      .withColumn("botSign",
        when(
          'total >= detectionSettings.activityThreshold,
          concat(
            lit(s"Total activity threshold is exceeded by event num: "),
            'total,
            lit(" in "),
            'delta,
            lit(" sec")
          )
        ) when(
          'clickRate >= detectionSettings.maxClickRate and 'total >= detectionSettings.minEstimationCount,
          concat(
            lit("Click rate threshold is exceeded by rate click / total: "),
            'clickRate,
            lit(" within "),
            'total,
            lit(" events in "),
            'delta,
            lit(" sec")
          )
        ) otherwise null)
      .filter('botSign.isNotNull)
      .select('ip, 'botSign as "cause", 'end cast LongType as "detected_timestamp")
      .as[BotItem]
  }

}

object StructureStreamingDetector {

  def apply(config: Config): StructureStreamingDetector = {
    new StructureStreamingDetector(
      detectionSettings = DetectionSettings(config getConfig "detection"),
      windowSettings = WindowSettings(config)
    )
  }
}
