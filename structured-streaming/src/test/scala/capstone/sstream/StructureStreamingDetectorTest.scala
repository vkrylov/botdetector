package capstone.sstream

import capstone.domain.Domain.{BotItem, Event}
import com.holdenkarau.spark.testing.DataFrameSuiteBase
import org.scalatest.FlatSpecLike

class StructureStreamingDetectorTest extends FlatSpecLike with DataFrameSuiteBase {

  behavior of classOf[StructureStreamingDetector].getSimpleName

  it should "find bots" in {
    import spark.implicits._

    val events = spark
      .createDataFrame(Seq(
        Event("click", "ip1", "1", "stub"),
        Event("view", "ip1", "2", "stub"),
        Event("click", "ip1", "3", "stub"),
        Event("view", "ip1", "4", "stub"), // <- click rate

        Event("click", "ip2", "1", "stub"),
        Event("view", "ip2", "2", "stub"),
        Event("view", "ip2", "3", "stub"),
        Event("view", "ip2", "4", "stub"),
        Event("click", "ip2", "5", "stub") // <- activity threshold
      ))
      .as[Event]

    val actual = new StructureStreamingDetector(DetectionSettings(5, 0.4, 4), WindowSettings("5 seconds", "1 second", "1 second"))
      .run(events)
      .distinct()
      .sort('detected_timestamp, 'ip)

    val expected = spark.createDataFrame(Seq(
      BotItem("ip1", "Click rate threshold is exceeded by rate click / total: 0.5 within 4 events in 4 sec", 4L),
      BotItem("ip2", "Total activity threshold is exceeded by event num: 5 in 5 sec", 5L)
    ))

    assertDataFrameEquals(expected, actual.toDF())
  }

}
