package capstone.domain

import capstone.domain.Domain.Activity
import cats.Eq
import cats.kernel.laws.discipline.SemigroupTests
import cats.tests.CatsSuite
import org.scalacheck.Arbitrary
import org.scalacheck.Arbitrary.arbitrary

class ActivitySemigroupTest extends CatsSuite {

  implicit val arbState = Arbitrary {
    for {
      s <- arbitrary[Long]
      e <- arbitrary[Long]
      cc <- arbitrary[Int]
      wc <- arbitrary[Int]
    } yield Activity(s, e, cc, wc)
  }

  implicit val eq: Eq[Activity] = Eq.fromUniversalEquals

  checkAll("ActivitySemigroup laws", SemigroupTests[Activity].semigroup)
}
