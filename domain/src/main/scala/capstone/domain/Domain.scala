package capstone.domain

import cats.Semigroup

object Domain {

  case class Event(`type`: String, ip: String, unix_time: String, url: String)

  case class Activity(start: Long, end: Long, clickCount: Int, watchCount: Int)

  object Activity {

    implicit val activitySemigroup = new Semigroup[Activity] {
      override def combine(x: Activity, y: Activity) = {
        Activity(
          start = x.start min y.start,
          end = x.end max y.end,
          clickCount = x.clickCount + y.clickCount,
          watchCount = x.watchCount + y.watchCount
        )
      }
    }

    def apply(event: Event): Activity = {
      val t = event.unix_time.toLong
      val isClick = event.`type` == "click"
      Activity(t, t, if (isClick) 1 else 0, if (isClick) 0 else 1)
    }
  }

  case class BotItem(ip: String, cause: String, detected_timestamp: java.lang.Long)
}
