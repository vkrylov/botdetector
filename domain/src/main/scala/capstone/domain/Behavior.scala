package capstone.domain

sealed trait Behavior

object Behavior {
  case object Normal extends Behavior

  trait Bot extends Behavior {
    def description: String
  }

  case class ActivityThreshold(num: Int, sec: Long) extends Bot {
    val description = s"Total activity threshold is exceeded by event num: $num in $sec sec"
  }

  case class ClickRate(rate: Double, sec: Long, total: Int) extends Bot {
    val description = s"Click rate threshold is exceeded by rate click / total: $rate within $total events in $sec sec"
  }
}

