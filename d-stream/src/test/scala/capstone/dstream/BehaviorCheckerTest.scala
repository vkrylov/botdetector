package capstone.dstream

import capstone.domain.Behavior.{ActivityThreshold, ClickRate, Normal}
import capstone.domain.Domain.Activity
import org.scalatest.Matchers

class BehaviorCheckerTest extends org.scalatest.FlatSpecLike with Matchers {

  behavior of classOf[BehaviorChecker].getSimpleName

  it should "check threshold" in {
    val tc = new BehaviorChecker(20, 0.25, 10)

    tc indicate Activity(0, 500, 0, 20) shouldBe ActivityThreshold(20, 500)
    tc indicate Activity(0, 500, 10, 10) shouldBe ActivityThreshold(20, 500)
    tc indicate Activity(0, 500, 20, 0) shouldBe ActivityThreshold(20, 500)
    tc indicate Activity(0, 500, 4, 12) shouldBe ClickRate(0.25, 500, 16)
    tc indicate Activity(0, 500, 2, 0) shouldBe Normal
    tc indicate Activity(0, 500, 0, 0) shouldBe Normal
    tc indicate Activity(0, 500, 0, 15) shouldBe Normal
    tc indicate Activity(0, 500, 2, 10) shouldBe Normal
  }
}
