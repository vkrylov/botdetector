package capstone.dstream

import capstone.domain.Behavior.{ActivityThreshold, ClickRate, Normal}
import capstone.domain.Domain.{Activity, BotItem, Event}
import com.holdenkarau.spark.testing.StreamingSuiteBase
import org.apache.spark.streaming.Seconds
import org.scalatest.FlatSpecLike

class BotDetectorTest extends FlatSpecLike with StreamingSuiteBase {

  behavior of classOf[BotDetector].getSimpleName

  it should "convert to bots" in {
    val actual = Seq(Seq(
      "ip1" -> ClickRate(0.1, 2, 3),
      "ip2" -> Normal,
      "ip3" -> ActivityThreshold(4, 5)
    ))

    val expected = Seq(Seq(
      BotItem("ip1", "Click rate threshold is exceeded by rate click / total: 0.1 within 3 events in 2 sec", 1000L),
      BotItem("ip3", "Total activity threshold is exceeded by event num: 4 in 5 sec", 1000L)
    ))

    testOperation(actual, BotDetector.toBots, expected)
  }

  it should "convert to activities" in {
    val actual = Seq(
      Seq(
        "ip1" -> Event("click", "stub", "1", "stub"),
        "ip2" -> Event("view", "stub", "1", "stub")
      ),
      Seq(
        "ip1" -> Event("click", "stub", "2", "stub"),
        "ip1" -> Event("view", "stub", "4", "stub")
      )
    )

    val expected = Seq(
      Seq(
        "ip1" -> Activity(1, 4, 2, 1),
        "ip2" -> Activity(1, 1, 0, 1)
      )
    )

    val sc = StreamConfig(Seconds(2), Seconds(2))

    testOperation(actual, BotDetector.toActivities(sc), expected)
  }
}

