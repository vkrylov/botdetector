package capstone.dstream

import com.typesafe.config.Config
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.streaming.StreamingContext
import org.apache.spark.streaming.dstream.InputDStream
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import org.apache.spark.streaming.kafka010.KafkaUtils
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent

import scala.collection.JavaConversions._

trait KafkaDStreamFactory {

  def config: Config

  def streamingContext: StreamingContext

  def createDStream(): InputDStream[ConsumerRecord[String, String]] = {
    val kafkaConfig = config.getConfig("kafka")

    val kafkaParams = Map(
      "bootstrap.servers" -> kafkaConfig.getStringList("bootstrap.servers").mkString(" "),
      "key.deserializer" -> classOf[StringDeserializer],
      "value.deserializer" -> classOf[StringDeserializer],
      "group.id" -> kafkaConfig.getString("group.id"),
      "auto.offset.reset" -> kafkaConfig.getString("offset.reset"),
      "enable.auto.commit" -> (false: java.lang.Boolean)
    )
    val topics = kafkaConfig.getStringList("topics").toSet

    KafkaUtils.createDirectStream[String, String](
      streamingContext,
      PreferConsistent,
      Subscribe[String, String](topics, kafkaParams)
    )
  }

}
