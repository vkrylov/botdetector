package capstone.dstream

import capstone.domain.Behavior
import capstone.domain.Behavior.Bot
import capstone.domain.Domain.{Activity, BotItem, Event}
import capstone.dstream.BotDetector.{Context, _}
import capstone.utils.JsonUtils
import cats.implicits._
import com.datastax.spark.connector.streaming._
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.spark.streaming.StreamingContext
import org.apache.spark.streaming.dstream.{DStream, InputDStream}

import scala.util.Try

class BotDetector(ctx: Context) {

  def createStream(): Unit = {
    val sc = ctx.streamConfig

    val findBotFunction = readEvents >>> toActivities(sc) >>> toBehavior(ctx.behaviorChecker.indicate) >>> toBots

    val bots = findBotFunction apply ctx.createDStream()

    bots.saveToCassandra("detector", "bot")

    bots.print()
  }

}

object BotDetector {

  trait Context {
    def behaviorChecker: BehaviorChecker

    def streamConfig: StreamConfig

    def createDStream(): InputDStream[ConsumerRecord[IP, String]]

    def streamingContext: StreamingContext
  }

  type IP = String
  type DStreamWith[IP, B] = DStream[(IP, B)]

  val jacksonParser: String => Option[Event] = { event =>
    Try {
      JsonUtils.mapper.readValue[Event](event)
    }.toOption
  }

  val circeParser: String => Option[Event] = { event =>
    import io.circe.jackson._
    import io.circe.generic.auto._
    import cats.syntax.either._

    decode[Event](event).toOption
  }

  val readEvents: DStream[ConsumerRecord[IP, String]] => IP DStreamWith Event = { record =>
    record
      .flatMap(r => circeParser(r.value()).map(e => r.key() -> e))
  }

  val toActivities: StreamConfig => IP DStreamWith Event => IP DStreamWith Activity = {
    sc => {
      _
        .mapValues(Activity.apply)
        .reduceByKeyAndWindow((_: Activity) |+| (_: Activity), sc.window, sc.slide)
        .checkpoint(sc.slide * 5)
    }
  }

  val toBehavior: (Activity => Behavior) => IP DStreamWith Activity => IP DStreamWith Behavior = {
    bf => {
      _ mapValues bf
    }
  }

  val toBots: IP DStreamWith Behavior => DStream[BotItem] = { bs =>
    bs
      .flatMapValues {
        case v: Bot => Some(v)
        case _ => None
      }
      .transform((rdd, t) => {
        rdd.mapValues(_ -> t)
      })
      .map(r => {
        val (ip, (b, t)) = r
        BotItem(ip, b.description, t.milliseconds)
      })
  }
}