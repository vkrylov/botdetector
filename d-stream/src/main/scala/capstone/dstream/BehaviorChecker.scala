package capstone.dstream

import capstone.domain.Behavior
import capstone.domain.Behavior._
import capstone.domain.Domain.Activity
import com.typesafe.config.Config

case class BehaviorChecker(activityThreshold: Int, clickRate: Double, minCount: Int) {

  def indicate(status: Activity): Behavior = {
    val cs = status.clickCount
    val wc = status.watchCount

    def delta = status.end - status.start

    val totalActivity = cs + wc
    val cr = cs.toDouble / totalActivity
    if (totalActivity >= activityThreshold) {
      ActivityThreshold(totalActivity, delta)
    } else if (cr >= clickRate && totalActivity >= minCount) {
      ClickRate(cr, delta, totalActivity)
    } else {
      Normal
    }
  }

}

object BehaviorChecker {
  def apply(config: Config): BehaviorChecker = {
    val c = config getConfig "detection"
    val at = c getInt "activity.threshold"
    val cr = c getDouble "click.max.rate"
    val mc = c getInt "estimation.min.count"
    new BehaviorChecker(at, cr, mc)
  }
}