package capstone.dstream

import com.typesafe.config.Config

case class DetectionSettings(activityThreshold: Int, maxClickRate: Double, minEstimationCount: Int)

object DetectionSettings {

  def apply(config: Config): DetectionSettings = {
    new DetectionSettings(
      activityThreshold = config getInt "activity.threshold",
      maxClickRate = config getDouble "click.max.rate",
      minEstimationCount = config getInt "estimation.min.count"
    )
  }

}