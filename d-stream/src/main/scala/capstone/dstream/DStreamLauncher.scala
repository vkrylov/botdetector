package capstone.dstream

import capstone.dstream.BotDetector.Context
import capstone.dstream.TimeUtils._
import com.typesafe.config.{Config, ConfigFactory}
import org.apache.log4j.Logger
import org.apache.spark.SparkConf
import org.apache.spark.streaming.StreamingContext

import scala.collection.JavaConversions._

object DStreamLauncher extends App {
  val log = Logger getLogger getClass

  val appConfig = ConfigFactory.load getConfig "capstone.botdetector"
  val checkpointDir = appConfig getString "checkpoint.dir"

  val ssc = StreamingContext.getOrCreate(checkpointDir, () => {

    val ctx = new Context with KafkaDStreamFactory {
      lazy val config: Config = appConfig

      lazy val streamConfig = StreamConfig(config)

      lazy val behaviorChecker = BehaviorChecker(config)

      lazy val streamingContext: StreamingContext = {
        val sc = new SparkConf(true)
          .setMaster("local[*]")
          .setAppName("BotDetector")
          .set("spark.cassandra.connection.host", config getStringList "cassandra.hosts" mkString ",")
          .set("spark.cassandra.connection.port", config getString "cassandra.port")

        val ssc = new StreamingContext(sc, config sparkDuration "batch")
        ssc checkpoint checkpointDir
        ssc
      }
    }

    new BotDetector(ctx).createStream()

    ctx.streamingContext
  })

  ssc.start()
  log info "Stream reading started"
  ssc.awaitTermination()
}
