package capstone.dstream

import com.typesafe.config.Config
import org.apache.spark.streaming.Duration

object TimeUtils {

  implicit class ConfigOps(c: Config) {
    def sparkDuration(p: String): Duration = {
      c.getDuration(p).asSpark
    }
  }

  implicit class DurationOps(d: java.time.Duration) {
    def asSpark: Duration = {
      Duration(d.toMillis)
    }
  }

}
