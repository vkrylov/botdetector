package capstone.dstream

import com.typesafe.config.Config
import org.apache.spark.streaming.Duration
import TimeUtils._

case class StreamConfig(window: Duration, slide: Duration)

object StreamConfig {
  def apply(config: Config): StreamConfig = {
    StreamConfig(
      config sparkDuration "window",
      config sparkDuration "slide"
    )
  }
}
