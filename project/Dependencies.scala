import sbt._

object Dependencies {

  lazy val catsVersion = "1.0.1"

  lazy val catsDependencies = Seq(
    "org.typelevel" %% "cats-core" % catsVersion,
    "org.typelevel" %% "cats-laws" % catsVersion % Test,
    "org.typelevel" %% "cats-testkit" % catsVersion % Test
  )

  lazy val scalatest = "org.scalatest" %% "scalatest" % "3.0.4" % Test

  lazy val typesafeConfig = "com.typesafe" % "config" % "1.3.2"

  lazy val sparkVersion = "2.2.1"

  lazy val sparkCore = "org.apache.spark" %% "spark-core" % sparkVersion

  lazy val cassandraConnector = "com.datastax.spark" %% "spark-cassandra-connector" % "2.0.7"

  lazy val sparkTestingBase = Seq(
    "com.holdenkarau" %% "spark-testing-base" % "2.2.0_0.8.0"  % Test,
    "org.apache.spark" %% "spark-hive" % sparkVersion % Test
  )

  lazy val commonDeps = catsDependencies ++ Seq(
    scalatest,
    typesafeConfig,
    sparkCore,
    cassandraConnector
  ) ++ sparkTestingBase
}
