package capstone.utils

import java.time.{Duration, Instant, LocalDateTime, ZoneOffset}

import capstone.domain.Domain.Event
import com.google.common.io.Files

import scala.util.{Random, Try}

object DataFileGenerator extends App {
  val tmpFileName = "data/records.dat"
  val resultFileName = "data/1.log"

  val tmpFile = new java.io.File(tmpFileName)

  tmpFile.getParentFile.mkdirs()

  val startApp = System.currentTimeMillis()

  val pw = new java.io.PrintWriter(tmpFile)

  var total = 0

  val writeResult = Try {
    val tillUnixTime = LocalDateTime.of(2018, 2, 16, 14, 20, 35, 234000).toInstant(ZoneOffset.UTC)
    val duration = Duration.ofMinutes(10)

    val start = tillUnixTime.minus(duration).getEpochSecond
    val end = tillUnixTime.getEpochSecond

    for {
      s <- start.until(end, 60)
    } {
      val i = Instant.ofEpochSecond(s)

      val bots = Seq(
        activity(Click, i, "0.0.0.1", 100),
        activity(View, i, "0.0.0.1", 200),
        activity(Click, i, "0.0.0.2", 100),
        activity(View, i, "0.0.0.2", 400),
        activity(Click, i, "0.0.0.3", 2000),
        activity(View, i, "0.0.0.4", 2000)
      )

      val users = for {
        a <- 1 to 10
        d <- 1 to 52
        at = if ((a + d) % 4 == 0) Click else View
        act <- activity(at, i, s"$a.$a.$d.$d", 10)
      } yield act

      Random.shuffle(bots.flatten ++ users).foreach { e =>
        val json = JsonUtils.mapper.writeValueAsString(e)
        pw.println(json)
        total += 1
      }
    }
  }

  writeResult
    .flatMap(_ => Try {
      val resFile = new java.io.File(resultFileName)
      resFile.getParentFile.mkdirs()
      Files.move(tmpFile, resFile)
    })
    .recover {
      case e: Exception => e.printStackTrace()
    }
    .foreach(_ => pw.close())

  println(s"$total items were written to $resultFileName in ${System.currentTimeMillis() - startApp} ms")

  trait ActivityType {
    def description: String
  }

  object Click extends ActivityType {
    def description = "click"
  }

  object View extends ActivityType {
    def description = "view"
  }

  def activity(t: ActivityType, time: Instant, ip: String, num: Int) = {
    val url = "https://blog.griddynamics.com/in-stream-processing-service-blueprint"
    Seq.fill(num) {
      Event(t.description, ip, time.getEpochSecond.toString, url)
    }
  }
}
