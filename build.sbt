import Dependencies._

lazy val commonSettings = Seq(
  organization := "com.example",
  version := "0.1-SNAPSHOT",
  scalaVersion := "2.11.12"
)

lazy val root = (project in file("."))
  .dependsOn(structured, dStream)
  .settings(commonSettings, libraryDependencies ++= commonDeps)

lazy val domain = (project in file("domain"))
  .settings(commonSettings, libraryDependencies ++= commonDeps)

lazy val utils = (project in file("utils"))
  .settings(commonSettings, libraryDependencies ++= commonDeps)
  .dependsOn(domain)

lazy val structured = (project in file("structured-streaming"))
  .settings(commonSettings, libraryDependencies ++= commonDeps)
  .dependsOn(domain, utils)

lazy val dStream = (project in file("d-stream"))
  .settings(commonSettings, libraryDependencies ++= commonDeps)
  .dependsOn(domain, utils)
